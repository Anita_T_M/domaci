-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: 127.0.0.1    Database: DZ_6
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drzava`
--

DROP TABLE IF EXISTS `drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drzava` (
  `kod` varchar(3) NOT NULL,
  `naziv` varchar(45) NOT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drzava`
--

LOCK TABLES `drzava` WRITE;
/*!40000 ALTER TABLE `drzava` DISABLE KEYS */;
INSERT INTO `drzava` VALUES ('BIH','Bosna i Hercegovina'),('SLO','Slovenija'),('SRB','Srbija');
/*!40000 ALTER TABLE `drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grad`
--

DROP TABLE IF EXISTS `grad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grad` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `pttBroj` varchar(10) NOT NULL,
  `naziv` varchar(45) NOT NULL,
  `drzavaID` varchar(3) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_grad_1_idx` (`drzavaID`),
  CONSTRAINT `fk_grad_1` FOREIGN KEY (`drzavaID`) REFERENCES `drzava` (`kod`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grad`
--

LOCK TABLES `grad` WRITE;
/*!40000 ALTER TABLE `grad` DISABLE KEYS */;
INSERT INTO `grad` VALUES (1,'21000','Novi Sad','SRB'),(2,'11000','Beograd','SRB'),(3,'18000','Nis','SRB'),(4,'1000','Slovenija','SLO'),(5,'78000','Banja Luka','BIH'),(6,'71000','Sarajevo','BIH');
/*!40000 ALTER TABLE `grad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorija`
--

DROP TABLE IF EXISTS `kategorija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorija` (
  `kategorijaID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  PRIMARY KEY (`kategorijaID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorija`
--

LOCK TABLES `kategorija` WRITE;
/*!40000 ALTER TABLE `kategorija` DISABLE KEYS */;
INSERT INTO `kategorija` VALUES (6554561,'garderoba'),(87984764,'basta');
/*!40000 ALTER TABLE `kategorija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica`
--

DROP TABLE IF EXISTS `prodavnica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica` (
  `prodavnicaID` int(11) NOT NULL,
  `naziv` varchar(45) NOT NULL,
  `gradID` int(11) NOT NULL,
  PRIMARY KEY (`prodavnicaID`),
  KEY `fk_prodavnica_1_idx` (`gradID`),
  CONSTRAINT `fk_prodavnica_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica`
--

LOCK TABLES `prodavnica` WRITE;
/*!40000 ALTER TABLE `prodavnica` DISABLE KEYS */;
INSERT INTO `prodavnica` VALUES (12315,'KIWI',1),(121456,'UNIVER',4),(158463,'LUSTER CO',2),(758964,'AKSA',5),(4511369,'MARKOM',3),(7845142,'NOVA',6);
/*!40000 ALTER TABLE `prodavnica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica_proizvod`
--

DROP TABLE IF EXISTS `prodavnica_proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica_proizvod` (
  `proizvodID` int(11) NOT NULL,
  `prodavnicaID` int(11) NOT NULL,
  `cena` decimal(12,2) NOT NULL,
  KEY `fk_prodavnica_proizvod_1_idx` (`prodavnicaID`),
  KEY `fk_prodavnica_proizvod_2_idx` (`proizvodID`),
  CONSTRAINT `fk_prodavnica_proizvod_1` FOREIGN KEY (`prodavnicaID`) REFERENCES `prodavnica` (`prodavnicaID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prodavnica_proizvod_2` FOREIGN KEY (`proizvodID`) REFERENCES `proizvod` (`proizvodID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica_proizvod`
--

LOCK TABLES `prodavnica_proizvod` WRITE;
/*!40000 ALTER TABLE `prodavnica_proizvod` DISABLE KEYS */;
INSERT INTO `prodavnica_proizvod` VALUES (1315,12315,879.00),(123456,121456,509.00);
/*!40000 ALTER TABLE `prodavnica_proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvod`
--

DROP TABLE IF EXISTS `proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvod` (
  `proizvodID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `barkod` varchar(13) NOT NULL,
  `proizvodjacID` int(11) NOT NULL,
  `kategorijaID` int(11) NOT NULL,
  PRIMARY KEY (`proizvodID`),
  KEY `fk_proizvodjacID_idx` (`proizvodjacID`),
  KEY `fk_proizvod_2_idx` (`kategorijaID`),
  CONSTRAINT `fk_proizvod_1` FOREIGN KEY (`proizvodjacID`) REFERENCES `proizvodjac` (`proizvodjacID`),
  CONSTRAINT `fk_proizvod_2` FOREIGN KEY (`kategorijaID`) REFERENCES `kategorija` (`kategorijaID`)
) ENGINE=InnoDB AUTO_INCREMENT=123457 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod`
--

LOCK TABLES `proizvod` WRITE;
/*!40000 ALTER TABLE `proizvod` DISABLE KEYS */;
INSERT INTO `proizvod` VALUES (1315,'Kapa','131231456',4512136,6554561),(123456,'Klupa','123124654',4512137,87984764);
/*!40000 ALTER TABLE `proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvodjac`
--

DROP TABLE IF EXISTS `proizvodjac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvodjac` (
  `proizvodjacID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(100) NOT NULL,
  `gradID` int(11) NOT NULL,
  PRIMARY KEY (`proizvodjacID`),
  KEY `fk_proizvodjac_1_idx` (`gradID`),
  CONSTRAINT `fk_proizvodjac_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4512142 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvodjac`
--

LOCK TABLES `proizvodjac` WRITE;
/*!40000 ALTER TABLE `proizvodjac` DISABLE KEYS */;
INSERT INTO `proizvodjac` VALUES (4512136,'Coloseum',1),(4512137,'Bezbriga',2),(4512138,'Duga',3),(4512139,'Krojac',4),(4512140,'Vil CO',5),(4512141,'Pomocnik',6);
/*!40000 ALTER TABLE `proizvodjac` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-19 14:46:18


SELECT * FROM proizvod 
JOIN drzava;

SELECT  COUNT('proizvodID') from proizvod;

SELECT * FROM prodavnica
WHERE prodavnica.gradID = 1;

SELECT * FROM proizvod 
join prodavnica
WHERE prodavnica.naziv = 'UNIVER';

SELECT * FROM proizvod 
join kategorija
WHERE kategorija.naziv = 'basta';

SELECT * FROM proizvod 
join kategorija
WHERE count(naziv) = 'basta';






